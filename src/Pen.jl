module Pen

using Random

include("load.jl")

x3 = 5

function go()
    labels = load_labels("data/train-labels-idx1-ubyte")
    images = load_images("data/train-images-idx3-ubyte")

    (images, labels)
end

function params(h, w)
    m = h * w

    i1 = 1 # A1
    i2 = i1 + m * m # b1
    i3 = i2 + m # A2
    i4 = i3 + m * 10 # b2
    i5 = i4 + 10 # end

    p = zeros(i5 - 1)

    va1 = @view p[i1:i2 - 1]
    vb1 = @view p[i2:i3 - 1]
    va2 = @view p[i3:i4 - 1]
    vb2 = @view p[i4:i5 - 1]

    a1 = reshape(va1, (m, m))
    b1 = reshape(vb1, m)
    a2 = reshape(va2, (10, m))
    b2 = reshape(vb2, 10)

    (p, a1, b1, a2, b2)
end

σ(x) = exp(x) / (exp(x) + 1)

σ1(x) = exp(x) / (exp(x) + 1)^2

function fwd(img, a1, b1, a2, b2)
    a2' * σ.(a1' * vec(img) + b1) + b2
end

function loss(img, a1, b1, a2, b2, l)
    l1 = argmax(a2' * σ.(a1' * vec(img) + b1) + b2) - 1
    convert(Int, l1 == l)
end

squared(x) = x^2

function gg()
    (images, labels) = Pen.load_batch("data/train-images-idx3-ubyte", "data/train-labels-idx1-ubyte")

    A1 = rand(784, 784)
    b1 = rand(784)
    A2 = rand(10, 784)
    b2 = rand(10)

    A1 *= 0.001
    b1 *= 0.001
    A2 *= 0.001
    b2 *= 0.001

    a0 = hcat(vec.(images)...)

    ns = size(a0, 2)
    nd = 10

    for step = 1:10
        println("Step $step")

        bb1 = hcat([ b1 for _ = 1:ns ]...)

        z1 = A1 * a0 + bb1

        a1 = σ.(z1)

        bb2 = hcat([ b2 for _ = 1:ns ]...)

        z2 = A2 * a1 + bb2

        a2 = σ.(z2)

        y = [ labels[i] == j - 1 for j = 1:nd, i = 1:ns ]

        loss = sum(squared.(a2 - y))

        println("Loss = $loss")

        # println("a2")
        # display(a2)
        # println("z2")
        # display(z2)
        # println("A2")
        # display(A2)
        # println("z1")
        # display(z1)

        u = [
            sum((a2[i, j] - y[i, j]) * σ1(z2[i, j]) * A2[i, m] * σ1(z1[m, j]) * a0[n, j] for i = 1:nd, j = 1:ns)
                for m = 1:784, n = 1:784
        ]

        p = [
            sum((a2[i, j] - y[i, j]) * σ1(z2[i, j]) * A2[i, m] * σ1(z1[m, j]) for i = 1:nd, j = 1:ns)
                for m = 1:784
        ]

        v = [
            sum((a2[m, j] - y[m, j]) * σ1(z2[m, j]) * a1[n, j] for j = 1:ns)
                for m = 1:nd, n = 1:784
        ]

        q = [
            sum((a2[m, j] - y[m, j]) * σ1(z2[m, j]) for j = 1:ns)
                for m = 1:nd
        ]

        display(u) # A1
        display(p) # b1
        display(v) # A2
        display(q) # b2

        println("Update parameters...")

        A1 += u * 0.1
        b1 += p * 0.1
        A2 += v * 0.1
        b2 += q * 0.1
    end
end

end # module
