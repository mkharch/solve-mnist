function load_labels(datafile::String)
    f = open(datafile)
    read(f, 4) # magic

    b = convert(Vector{Int32}, read(f, 4))
    n = (b[1] << 24) + (b[2] << 16) + (b[3] << 8) + b[4]

    labels = convert(Vector{Int}, read(f, n))
    close(f)

    labels
end

function load_images(datafile::String)
    f = open(datafile)
    read(f, 4) # magic

    b = convert(Vector{Int32}, read(f, 4))
    n = (b[1] << 24) + (b[2] << 16) + (b[3] << 8) + b[4]

    b = convert(Vector{Int32}, read(f, 4))
    h = (b[1] << 24) + (b[2] << 16) + (b[3] << 8) + b[4]
    b = convert(Vector{Int32}, read(f, 4))
    w = (b[1] << 24) + (b[2] << 16) + (b[3] << 8) + b[4]

    images = Vector{Matrix{Float64}}()
    for _ = 1:n
        m = reshape(read(f, h * w), (h, w))'
        push!(images, m ./ 256)
    end

    close(f)
    images
end

function load_batch(imagefile::String, labelfile::String, bsize=100)
    f1 = open(imagefile)

    read(f1, 4) # magic
    n = ntoh(read(f1, Int32))
    @assert n == 60000
    h = ntoh(read(f1, Int32))
    w = ntoh(read(f1, Int32))
    @assert h == 28 && w == 28

    images = Vector{Matrix{Float64}}()
    for _ = 1:bsize
        m = reshape(read(f1, h * w), (h, w))'
        push!(images, m ./ 256)
    end

    close(f1)

    f2 = open(labelfile)

    read(f2, 4) # magic
    n = ntoh(read(f2, Int32))
    @assert n == 60000

    labels = convert(Vector{Int}, read(f2, bsize))
    close(f2)

    (images, labels)
end